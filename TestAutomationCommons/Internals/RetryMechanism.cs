﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Internals
{
    public class RetryMechanism
    {
        private List<Type> IgnoredExceptions;
        private TimeSpan Duration { get; set; }
        private TimeSpan Interval { get; set; }
        private readonly TimeSpan ZeroTime = TimeSpan.FromSeconds(0);
        public RetryMechanism()
        {
            IgnoredExceptions = new List<Type>();
            var duration = ConfigurationManager.AppSettings.Get("retry-duration-seconds");
            var interval = ConfigurationManager.AppSettings.Get("retry-interval-seconds");
            duration ??= "12";
            interval ??= "1";
            try
            {
                Interval = TimeSpan.FromSeconds(Int32.Parse(interval));
                Duration = TimeSpan.FromSeconds(Int32.Parse(duration));
            }
            catch (FormatException nan)
            {
                throw new FormatException($"Either interval (`{interval}`) or duration (`{duration}`) is not a number", nan);
            }
        }

        public RetryMechanism Until(ThreadStart Condition)
        {
            DateTime MaxTime = DateTime.Now.Add(Duration);
            DateTime Elapsed = DateTime.Now;

            while (Elapsed.Subtract(MaxTime) < ZeroTime)
            {
                Elapsed = DateTime.Now;
                try
                {
                    Condition.Invoke();
                    break;
                }
                catch (Exception CaughtExeption)
                {
                    if (IgnoredExceptions.Contains(CaughtExeption.GetType()) && Elapsed.Subtract(MaxTime) < ZeroTime)
                        Thread.Sleep(Interval);
                    else throw CaughtExeption;
                }
            }
            return this;
        }

        public RetryMechanism WithDuration(TimeSpan newDuration)
        {
            Duration = newDuration;
            return this;
        }
        public RetryMechanism WithInterval(TimeSpan newInterval)
        {
            Interval = newInterval;
            return this;
        }
        public RetryMechanism IgnoreSeleniumExceptions()
        {
            IgnoreExceptions(
                typeof(WebDriverException),
                typeof(NoSuchElementException),
                typeof(StaleElementReferenceException),
                typeof(ElementClickInterceptedException),
                typeof(ElementNotInteractableException)
                );
            return this;
        }
        public RetryMechanism IgnoreExceptions(params Type[] types)
        {
            foreach (Type type in types)
            {
                IgnoredExceptions.Add(type);
            }
            return this;
        }
    }
}
