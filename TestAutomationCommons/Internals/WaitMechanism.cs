﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestAutomationCommons.Internals
{
    public class WaitMechanism
    {
        public WaitMechanism() { }

        public void ForSeconds(double seconds)
        {
            var duration = TimeSpan.FromSeconds(seconds);
            var until = DateTime.Now + duration;
            while (until > DateTime.Now)
            {
                //do nothing
            }
        }
    }
}
