﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorTest.ApplicationUnderTest
{
    public class Calculator
    {
        private double Answer;
        public double GetAnswer()
        {
            return Answer;
        }
        public Calculator Add(double a, double b)
        {
            Answer = a + b;
            return this;
        }

        public Calculator Subtract(double a, double b)
        {
            Answer = a - b;
            return this;
        }

        public Calculator Multply(double a, double b)
        {
            Answer = a * b;
            return this;
        }

        public Calculator Divide(double a, double b)
        {
            Answer = a / b;
            return this;
        }
    }
}
