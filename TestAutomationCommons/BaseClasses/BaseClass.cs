﻿using AutomationCommons.Providers;
using Internals;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;
using TechTalk.SpecFlow;
using TestAutomationCommons.Internals;

namespace AutomationCommons
{
    public class BaseClass
    {
        protected BaseClass(ScenarioContext ctx)
        {
            ScenarioContext = ctx;
        }
        protected ScenarioContext ScenarioContext { get; set; }
        protected String BaseURL
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("BaseUrl");
            }
            private set { }
        }
        protected RetryMechanism Retry => new RetryMechanism();
        protected WaitMechanism Wait => new WaitMechanism();
        protected DriverProvider DriverProvider => new DriverProvider(ScenarioContext);
        protected IWebDriver WebDriver => DriverProvider.GetWebDriver();
        protected WebDriverWait WebDriverWait => new WebDriverWait(WebDriver, TimeSpan.FromSeconds(5));

    }
}
