﻿using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace AutomationCommons.BaseClasses
{
    public class BaseStep : BaseClass
    {
        public BaseStep(ScenarioContext ctx) : base(ctx) { }
    }
}
