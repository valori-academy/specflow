﻿using AutomationCommons.BaseClasses;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace AutomationCommons.Providers
{
    public class PageProvider : BaseProvider
    {
        public PageProvider(ScenarioContext ctx) : base(ctx) { }
    }
}
