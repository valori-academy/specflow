﻿using AutomationCommons.Providers;
using OnlineCalculatorTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace OnlineCalculatorTest.Providers
{
    public class CalculatorPages : PageProvider
    {
        public CalculatorPages(ScenarioContext ctx) : base(ctx) {}
        public CalculatorPage CalculatorPage => new CalculatorPage(ScenarioContext);
    }
}
