﻿@calculation
Feature: Calculations
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@adding-numbers
Scenario Outline: Add two numbers
	Given I have opened the calculator
	When I add <first> and <second>
	Then the result should be <result>

	Examples: Adding numbers
		| first | second | result |
		| 50    | 70     | 120    |
		| 32    | 88     | 120    |
		| -10   | 7      | -3     |
		| 500   | 1000   | 1500   |

@subtracting-numbers
Scenario Outline: Subtract two numbers
	Given I have opened the calculator
	When I subtract <first> and <second>
	Then the result should be <result>

	Examples: Adding numbers
		| first | second | result |
		| 50    | 70     | -20    |
		| 32    | 88     | -56    |
		| -10   | 7      | -17    |
		| 212   | 100    | 112    |