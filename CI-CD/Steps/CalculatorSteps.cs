﻿using AutomationCommons.BaseClasses;
using CalculatorTest.ApplicationUnderTest;
using NFluent;
using OnlineCalculatorTest.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CalculatorTest.Steps
{
    [Binding]
    public sealed class CalculatorSteps : OnlineCalculatorStep
    {
        public CalculatorSteps(ScenarioContext ctx) : base(ctx)
        {
        }

        [Given(@"I have opened the calculator")]
        public void OpenCalculator()
        {
            Pages.CalculatorPage.OpenPage();
        }

        [When(@"I add (.*) and (.*)")]
        public void AddNumbers(double first, double second)
        {
            Pages.CalculatorPage.Calculate(first + " + " + second);
        }

        [When(@"I subtract (.*) and (.*)")]
        public void WhenISubtractAnd(double first, double second)
        {
            Pages.CalculatorPage.Calculate(first + " - " + second);
        }


        [Then(@"the result should be (.*)")]
        public void CheckResult(string expectedResult)
        {
            string actualAnswer = Pages.CalculatorPage.GetAnswer();
            Check.That(expectedResult).IsEqualTo(actualAnswer);
        }
    }
}
