﻿using AutomationCommons.BaseClasses;
using OnlineCalculatorTest.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace OnlineCalculatorTest.BaseClasses
{
    public class OnlineCalculatorStep : BaseStep
    {
        public OnlineCalculatorStep(ScenarioContext ctx) : base(ctx) {}
        public CalculatorPages Pages => new CalculatorPages(ScenarioContext);
    }
}
