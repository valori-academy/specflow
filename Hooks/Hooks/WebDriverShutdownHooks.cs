﻿using AutomationCommons;
using AutomationCommons.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Hooks.Hooks
{
    [Binding]
    public class WebDriverShutdownHooks : BasePage
    {
        protected WebDriverShutdownHooks(ScenarioContext ctx) : base(ctx) {}

        [AfterScenario]
        public void CloseAllBrowsers()
        {
            /* 
             * Use this method to execute code AFTER any Scenario. 
             * The DriverProvider is responsible for all browser interactions 
             */


        }
    }
}
