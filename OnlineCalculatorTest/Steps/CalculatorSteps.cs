﻿using AutomationCommons.BaseClasses;
using CalculatorTest.ApplicationUnderTest;
using NFluent;
using OnlineCalculatorTest.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CalculatorTest.Steps
{
    [Binding]
    public sealed class CalculatorSteps : OnlineCalculatorStep
    {
        private Calculator Calculator;
        public CalculatorSteps(ScenarioContext ctx) : base(ctx)
        {
        }

        [Given(@"I have opened the calculator")]
        public void OpenCalculator()
        {
            Calculator = new Calculator();
        }

        [When(@"I add (.*) and (.*)")]
        public void AddNumbers(double first, double second)
        {
            Calculator.Add(first, second);
        }

        [Then(@"the result should be (.*)")]
        public void CheckResult(double expectedResult)
        {
            double actualAnswer = Calculator.GetAnswer();
            Check.That(expectedResult).IsEqualTo(actualAnswer);
        }
    }
}
