﻿using AutomationCommons.BaseClasses;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace OnlineCalculatorTest.Pages
{
    public class CalculatorPage : BasePage
    {
        private IWebElement OmniBox => FindElement(By.Name("q"));
        private IWebElement Result => FindElement(By.Id("cwos"));
        public CalculatorPage(ScenarioContext ctx) : base(ctx) { }

        public CalculatorPage OpenPage()
        {
            WebDriver.Navigate().GoToUrl(BaseURL);
            return this;
        }
        public CalculatorPage Add(double first, double second)
        {
            var calculation = $"{first} + {second}";
            OmniBox.SendKeys(calculation);
            OmniBox.SendKeys(Keys.Enter);
            return this;
        }
        public double GetAnswer()
        {
            return Double.Parse(Result.Text);
        }
    }
}
