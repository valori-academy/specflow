﻿using Internals;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webshop.Models
{
    public class Product
    {
        private IWebElement ProductElement;
        public Product(IWebElement ProductElement)
        {
            this.ProductElement = ProductElement;
            Title = ProductElement.FindElement(By.TagName("h2")).Text;
            AddToCartButton = ProductElement.FindElement(By.LinkText("Add to cart"));
            try
            {
                Price = ProductElement.FindElement(By.ClassName("price")).FindElement(By.TagName("ins")).Text;
                OriginalPrice = ProductElement.FindElement(By.ClassName("price")).FindElement(By.TagName("del")).Text;
                OnSale = ProductElement.FindElement(By.ClassName("onsale")).Displayed;
            }
            catch// not on sale
            {
                Price = ProductElement.FindElement(By.ClassName("price")).FindElement(By.TagName("span")).Text;
            };

        }
        public string Title { get; private set; }
        public string Price { get; private set; }
        public string OriginalPrice { get; private set; }

        private readonly Boolean OnSale;
        private readonly IWebElement AddToCartButton;
        public Boolean IsOnSale() { return OnSale; }
        public void AddToCart()
        {
            AddToCartButton.Click();
            new RetryMechanism().IgnoreSeleniumExceptions().Until(() => {
                ProductElement.FindElement(By.ClassName("added_to_cart"));
            });

        }
        public void OpenProduct()
        {
            ProductElement.Click();
        }


    }
}
