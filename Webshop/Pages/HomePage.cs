﻿using AutomationCommons.BaseClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Webshop.Models;

namespace Webshop.Pages
{
    public class HomePage : BasePage
    {
        List<IWebElement> ProductList => FindElements(By.ClassName("type-product"));
        IWebElement CloseCookieBarButton => FindElement(By.ClassName("accept"));
        IReadOnlyCollection<IWebElement> CategoryList => FindElement(By.ClassName("product-categories")).FindElements(By.TagName("a"));
        List<IWebElement> MenuItems => FindElements(By.ClassName("menu-item"));
        IWebElement SearchBoxInput => FindElement(By.ClassName("search-field"));
        IWebElement CartPriceLabel => FindElement(By.ClassName("cart-contents")).FindElements(By.TagName("span"))[0];
        IWebElement CartItemsLabel => FindElement(By.ClassName("cart-contents")).FindElements(By.TagName("span"))[2];
        SelectElement SortSelection => new SelectElement(FindElement(By.Name("orderby")));

        public HomePage(ScenarioContext ctx) : base(ctx)
        {
        }
        public HomePage OpenPage()
        {
            WebDriver.Navigate().GoToUrl(BaseURL);
            return this;
        }
        public Product GetProduct(string productName)
        {
            var FoundProduct = GetProductList().Find(Product =>
            {
                return Product.Title.Equals(productName);
            });

            return FoundProduct;
        }
        public Product OpenProduct(string productName)
        {
            var FoundProduct = GetProductList().Find(Product =>
            {
                return Product.Title.Equals(productName);
            });
            FoundProduct.OpenProduct();
            return FoundProduct;
        }
        public HomePage OpenCategory(string categoryName)
        {
            CategoryList.ToList().Find(Category => Category.Text.Contains(categoryName)).Click();
            return this;
        }
        public void OpenCart()
        {
            MenuItems.Find(MenuItem => MenuItem.Text.Contains("Cart")).Click();
        }
        public HomePage SortBy(string sortType)
        {
            sortType = sortType.Replace("Sort by", String.Empty).Trim();

            SortSelection.SelectByText("Sort by " + sortType);
            return this;
        }
        public void Search(string productName)
        {
            SearchBoxInput.SendKeys(productName);
            SearchBoxInput.SendKeys(Keys.Enter);
        }
        public HomePage CloseCookieBar()
        {
            CloseCookieBarButton.Click();
            return this;
        }
        public List<Product> GetProductList()
        {
            var AllProducts = new List<Product>();
            ProductList.ForEach(Product => { AllProducts.Add(new Product(Product)); });
            return AllProducts;
        }
        public double GetCartPrice()
        {
            return Double.Parse(CartPriceLabel.Text.Replace("€", String.Empty).Trim());
        }
        public int GetNumberOfItemsInCart()
        {
            var number = CartItemsLabel.Text.Replace("items", String.Empty).Replace("item", String.Empty).Trim();
            return Int32.Parse(number);
        }
    }
}
