﻿using AutomationCommons.BaseClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using TechTalk.SpecFlow;

namespace AutomationCommons.Providers
{
    class DriverProvider : BaseProvider
    {
        private Uri WebDriverURI
        {
            get
            {
                return new Uri(ConfigurationManager.AppSettings.Get("WebDriverURI"));
            }
        }

        public DriverProvider(ScenarioContext ctx) : base(ctx) { }

        public IWebDriver GetWebDriver()
        {
            if (!ScenarioContext.ContainsKey("webDrivers"))
            {
                ScenarioContext.Add("webDrivers", new List<IWebDriver>());
                ScenarioContext.Add("activeWebDriverIndex", 0);
            }
            int activeWebDriverIndex = ScenarioContext.Get<int>("activeWebDriverIndex");
            SwitchWebDriver(activeWebDriverIndex);
            return ScenarioContext.Get<List<IWebDriver>>("webDrivers")[activeWebDriverIndex];
        }

        public void SwitchWebDriver(int index)
        {
            List<IWebDriver> webDrivers = ScenarioContext.Get<List<IWebDriver>>("webDrivers");
            //Create new
            if (index + 1 > webDrivers.Count || webDrivers.Count == 0)
            {
                webDrivers.Add(CreateWebDriverInstance());
            }
            ScenarioContext.Set(index, "activeWebDriverIndex");
            ScenarioContext.Set(webDrivers, "webDrivers");
        }

        private IWebDriver CreateWebDriverInstance()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("start-maximized"); // open Browser in maximized mode
            options.AddArguments("disable-infobars"); // disabling infobars
            options.AddArguments("--disable-extensions"); // disabling extensions
            options.AddArguments("--disable-gpu"); // applicable to windows os only
            options.AddArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            options.AddArguments("--no-sandbox"); // Bypass OS security model
            var webDriver = new RemoteWebDriver(WebDriverURI, options);
            webDriver.Manage().Window.Maximize();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            return webDriver;
        }
    }
}
