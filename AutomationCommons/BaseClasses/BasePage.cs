﻿using AutomationCommons.Providers;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using TechTalk.SpecFlow;

namespace AutomationCommons.BaseClasses
{
    class BasePage : BaseClass
    {
        BasePage(ScenarioContext ctx) : base(ctx) { }
        private DriverProvider DriverProvider => new DriverProvider(ScenarioContext);
        private IWebDriver WebDriver => DriverProvider.GetWebDriver();
        protected IWebElement FindElement(By element)
        {
            return FindElements(element)[0];
        }
        protected ReadOnlyCollection<IWebElement> FindElements(By element)
        {
            ReadOnlyCollection<IWebElement> foundElements = null;
            Retry
                .IgnoreSeleniumExceptions()
                .Until(() =>
                {
                    if (WebDriver.FindElements(element).Count > 0)
                        foundElements = WebDriver.FindElements(element);
                    else
                    {
                        WebDriver.SwitchTo().DefaultContent();
                        ReadOnlyCollection<IWebElement> iframes = WebDriver.FindElements(By.TagName("iframe"));
                        foreach (IWebElement iframe in iframes)
                        {
                            WebDriver.SwitchTo().Frame(iframe);
                            if (WebDriver.FindElements(element).Count > 0)
                            {
                                foundElements = WebDriver.FindElements(element);
                                return;
                            }
                            WebDriver.SwitchTo().DefaultContent();
                        }
                        throw new NoSuchElementException(element.ToString());
                    }
                });
            return foundElements;
        }
    }
}
