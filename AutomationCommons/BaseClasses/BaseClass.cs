﻿using Internals;
using System;
using System.Configuration;
using TechTalk.SpecFlow;

namespace AutomationCommons
{
    class BaseClass
    {
        protected BaseClass(ScenarioContext ctx)
        {
            ScenarioContext = ctx;
        }
        protected ScenarioContext ScenarioContext { get; set; }
        public String BaseURL
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("baseURL");
            }
        }
        protected RetryMechanism Retry => new RetryMechanism();

    }
}
