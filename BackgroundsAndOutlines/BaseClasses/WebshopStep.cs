﻿using AutomationCommons.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Webshop.Providers;

namespace Webshop.BaseClasses
{
    public class WebshopStep : BaseStep
    {
        public WebshopStep(ScenarioContext ctx) : base(ctx) { }
        public WebshopPages Pages => new WebshopPages(ScenarioContext);
    }
}
