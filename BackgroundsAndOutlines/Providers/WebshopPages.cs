﻿using AutomationCommons.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using Webshop.Pages;

namespace Webshop.Providers
{
    public class WebshopPages : BasePage
    {
        public WebshopPages(ScenarioContext ctx) : base(ctx) {}
        public HomePage HomePage => new HomePage(ScenarioContext);
        public CartPage CartPage => new CartPage(ScenarioContext);
    }
}
