﻿using AutomationCommons.BaseClasses;
using BackgroundsAndOutlines.Models;
using Internals;
using NFluent;
using NFluent.Kernel;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Webshop.Pages
{
    public class CartPage : BasePage
    {
        private string PageResource = "/index.php/cart/";

        private List<IWebElement> CartItemList => FindElements(By.ClassName("cart_item"));
        private IWebElement UpdateCartButton => FindElement(By.Name("update_cart"));
        private IWebElement TotalAmount => FindElement(By.XPath("//td[@data-title='Total']"));
        private IWebElement EmptyCartLabel => FindElement(By.ClassName("cart-empty"));
        private IWebElement ConfirmationMessage => FindElement(By.XPath("//div[@role='alert']"));
        public CartPage(ScenarioContext ctx) : base(ctx) { }

        internal void RemoveProduct(string productName)
        {
            var numberOfItems = GetCartItemList().Count;
            GetCartItemList().Find(CartItem => CartItem.Product.Equals(productName)).Remove();
            WebDriverWait.Until(driver =>
            {
                return ConfirmationMessage.Displayed;
            });
        }

        public CartPage OpenPage()
        {
            WebDriver.Navigate().GoToUrl(BaseURL + PageResource);
            return this;
        }

        public List<CartItem> GetCartItemList()
        {
            var FoundCartItemList = new List<CartItem>();

            CartItemList.ForEach(CartItem =>
            {
                FoundCartItemList.Add(new CartItem(CartItem));
            });
            return FoundCartItemList;
        }

        public string GetEmptyMessage()
        {
            return EmptyCartLabel.Text;
        }

        public int GetProductQuantity(string productName)
        {
            return GetCartItemList().Find(CartItem => CartItem.Product.Equals(productName)).Quantity;
        }

        public CartPage SetProductQuantity(string productName, int quantity)
        {
            GetCartItemList().Find(CartItem => CartItem.Product.Equals(productName)).SetQuantity(quantity);
            UpdateCart();
            WebDriverWait.Until(driver =>
            {
                return ConfirmationMessage.Displayed;
            });
            return this;
        }

        public double GetTotalAmount()
        {
            var totalAmount = TotalAmount.Text.Replace("€", String.Empty).Trim();
            return Double.Parse(totalAmount);
        }

        private CartPage UpdateCart()
        {
            UpdateCartButton.Click();
            new RetryMechanism().IgnoreSeleniumExceptions().Until(() =>
            {
                var enabled = UpdateCartButton.Enabled;
            });
            return this;
        }
    }
}
