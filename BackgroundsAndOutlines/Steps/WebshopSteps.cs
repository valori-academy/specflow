﻿using NFluent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Webshop.BaseClasses;

namespace Webshop.Steps
{
    [Binding]
    public class WebshopSteps : WebshopStep
    {
        public WebshopSteps(ScenarioContext ctx) : base(ctx) { }

        /* Paste the gluecode between this... */
        [Given(@"a customer is on the home page")]
        public void GivenACustomerIsOnTheHomePage()
        {
            Pages.HomePage
                .OpenPage()
                .CloseCookieBar();
        }

        [Given(@"he has added a (.*) to the cart")]
        public void AddProduct(string productName)
        {
            Pages.HomePage.GetProduct(productName).AddToCart();
            Wait.ForSeconds(3);
        }

        [Given(@"the cart is opened")]
        public void OpenCart()
        {
            Pages.HomePage.OpenCart();
        }

        [When(@"he removes the (.*) from its cart")]
        public void RemoveProduct(string productName)
        {
            Pages.CartPage.RemoveProduct(productName);
        }

        [Then(@"the cart is empty")]
        public void VerifyCartIsEmpty()
        {
            Pages.CartPage.OpenPage();
            var count = Pages.CartPage.GetEmptyMessage();
            Check.That(count).IsEqualTo("Your cart is currently empty.");
        }

        [When(@"he adds another (.*)")]
        public void AddProductFromCart(string productName)
        {
            var quantity = 1 + Pages.CartPage.GetProductQuantity(productName);
            Pages.CartPage.SetProductQuantity(productName, quantity);
        }

        [Then(@"the total amount is €(.*)")]
        public void VerifyTotalAmount(Decimal expectedAmount)
        {
            var amount = Pages.CartPage.GetTotalAmount();
            Check.That(amount).IsEqualTo(expectedAmount);
        }

        [When(@"he removes (.*) (.*)")]
        public void WhenHeRemovesBeanie(int reduction, string productName)
        {
            var quantity = Pages.CartPage.GetProductQuantity(productName) - reduction;
            Pages.CartPage.SetProductQuantity(productName, quantity);
        }

        /* ...and this line */
    }
}
