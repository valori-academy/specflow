﻿Feature: Cart
  The cart is responsible for storing products to buy, showing the total price and number of items.
  Customers are able to add and remove products from their cart.

Scenario: Removing Beanie from the cart
	Given a customer is on the home page
	And he has added a Beanie to the cart
	And the cart is opened
	When he removes the Beanie from its cart
	Then the cart is empty

Scenario: Higher quantity of Beanie
	Given a customer is on the home page
	And he has added a Beanie to the cart
	And the cart is opened
	When he adds another Beanie
	Then the total amount is €36.00

Scenario: Lower quantity of Beanie
	Given a customer is on the home page
	And he has added a Beanie to the cart
	And he has added a Beanie to the cart
	And the cart is opened
	When he removes 1 Beanie
	Then the total amount is €18.00

Scenario: Removing Belt from the cart
	Given a customer is on the home page
	Given he has added a Belt to the cart
	And the cart is opened
	When he removes the Belt from its cart
	Then the cart is empty

Scenario: Higher quantity of Belt
	Given a customer is on the home page 
	And he has added a Belt to the cart
	And the cart is opened
	When he adds another Belt
	Then the total amount is €110.00

Scenario: Lower quantity of Belt
	Given a customer is on the home page
	And he has added a Belt to the cart
	And he has added a Belt to the cart
	And the cart is opened
	When he removes 1 Belt
	Then the total amount is €55.00