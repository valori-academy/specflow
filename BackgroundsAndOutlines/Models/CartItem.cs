﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Webshop.Models;

namespace BackgroundsAndOutlines.Models
{
    public class CartItem
    {
        private readonly IWebElement CartItemElement;
        public string Price { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public string Total { get; set; }

        public CartItem(IWebElement CartItemElement)
        {
            this.CartItemElement = CartItemElement;
            Product = CartItemElement.FindElement(By.ClassName("product-name")).Text;
            Price = CartItemElement.FindElement(By.ClassName("product-price")).Text;
            Quantity = Int16.Parse(CartItemElement.FindElement(By.TagName("input")).GetAttribute("Value"));
            Total = CartItemElement.FindElement(By.ClassName("product-subtotal")).Text;
        }

        public void Remove()
        {
            CartItemElement.FindElement(By.ClassName("remove")).Click();
        }

        public CartItem SetQuantity(int quantity)
        {
            var quantityElement = CartItemElement.FindElement(By.TagName("input"));
            quantityElement.Clear();
            quantityElement.SendKeys(quantity.ToString());
            Quantity = Int16.Parse(quantityElement.GetAttribute("value"));
            return this;
        }
    }
}
